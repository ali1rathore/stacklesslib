Stacklesslib
===================

This is a pure python library providing support functionality for [Stackless
Python][1]

It provides synchronization primitives, event loops, context managers
and other goodies.


[1]: http://www.stackless.com "Stackless Python"